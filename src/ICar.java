/**
 * 
 **Different types in java
 *Class
 *enum
 *primitive
 *interface
 *
 *a class is a combination of data and method 
 *An interface only defines methods that is subclasses will have
 *By definition, everything in an interface is public
 *
 * @author unouser
 *
 */


public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
